$(document).ready(function(){

    // load highscore to save result modal 
    var ini=0;
    if(ini==0){
            var nameHighTest="";
            var loadPlayerDataTest = localStorage.getItem('my_playerData'); //typeof on the variable = string
            if(loadPlayerDataTest == null){
                
            } else{
               // prepare for showing highscore
                var nameAndScore = prepareNameScoreModals();
                var names = nameAndScore.sorteddata[0];
                var scores = nameAndScore.sorteddata[1];
                var counter =nameAndScore.datalength;
                var idName = nameAndScore.idName2;
                var idScore = nameAndScore.idScore2;
                // write to modal/show highscore
                for(var i=0; i<counter; i++){
                    $(idName[i]).html(names[i]); //jQuery instead of "document.getElementById("id").innerHTML=nameHightest;
                    $(idScore[i]).html(scores[i]);
                }  
                //make a string with best player name and score
                nameHighTest = "Name = " + names[0] + '<br>' + 'Score = ' + scores[0];     
                // show it on home page
                $("#demo").html(nameHighTest);            
            }
        $('#nameID').val(""); // set the value for element with id=nameID
        ini=1;
    }
   
// get youtube iframe API script ( AJAX: Asynchronous JavaScript and XML.)
    $.getScript('https://www.youtube.com/iframe_api');

    // NAVIGATION
    $('#HomeNavID').click(function(){
         $('.HomePageClass').show(50);
 
         $('.QuizPageClass').hide(50);
         player.stopVideo(); 
    });

       
    $('#QuizNavID').click(function(){
        $('.QuizPageClass').show(50);
        $('.movie-ready').show(50);
        $('#playModal').modal();

        $('.question-container[data-question="' + question + '"]').hide(50);
        question = 1;
        points = 0;

        $('.quiz-result').hide(50);
        $('.progress-bar').css('width', 0+'%').attr('aria-valuenow', 0);
        $('.progress').addClass('hidden');
     
        $('.quiz-ready').hide(50);
        $('.HomePageClass').hide(50);
    });

    $('#quiz-start').click(function() {
        $('.quiz-ready').hide(500);
        $('.question-container[data-question="1"]').show(500);
        $('.progress').removeClass('hidden');
        $("#saveButton").removeClass('hidden');
    });

    // QUIZ
    var question = 1;
    var points = 0;
    var percent;
    $('.answer').click(function() {
        points = points + $(this).data('points'); 

        $('.question-container[data-question="' + question + '"]').hide(500);
        question++;

        valeur = $('.question-container[data-question="' + question + '"]').data('step');
        $('.progress-bar').css('width', valeur+'%').attr('aria-valuenow', valeur);
    
        if(!$(this).parent().parent().hasClass('last-question')) {    
            $('.question-container[data-question="' + question + '"]').show(500);
        } else {
            $('.progress-bar').css('width',100 + '%').attr('aria-valuenow', 100);
            $('#points').html(points);
            percent = Math.round(points / ($('.quiz-result').data('max-result') / 100));
            $('#percent').html(percent);
            $('.passed').hide();
            $('.failed').hide();

            if(percent >= $('.quiz-result').data('needed')) {
                $('.passed').show();
            } else {
                $('.failed').show();
            }
            $('.quiz-result').show(500);
        }
    });

    $('#restart-quiz').click(function() {
        question = 1;
        points = 0;
        $('.quiz-result').hide(500);
        $('.question-container[data-question="' + question + '"]').show(500);
        $('.progress-bar').css('width', 0+'%').attr('aria-valuenow', 0);
        $("#saveButton").removeClass('hidden');
    });

    $('#restartClip').click(function() {
        question = 1;
        points = 0;
        $('.quiz-result').hide(500);
    
        $('.progress-bar').css('width', 0+'%').attr('aria-valuenow', 0);
        $('.progress').addClass('hidden');

        $('.movie-ready').show(50);
        $("#saveButton").removeClass('hidden');
        $('#playModal').modal();
    });

    // when saving result
    $('#saveResultID').click(function() {
        if(typeof(Storage) !== "undefined") { 
                var playerData_str = localStorage.getItem('my_playerData');
                if(playerData_str == null){
                    playerData=[];              
                } else{
                    playerData = JSON.parse(playerData_str);     
                }         
                playerData.push({"name": $("#nameID").val(), "score" : points}); 
                localStorage.setItem('my_playerData', JSON.stringify(playerData));
        }

        // prepare for showing highscore
        var nameAndScore = prepareNameScoreModals();
        var names = nameAndScore.sorteddata[0];
        var scores = nameAndScore.sorteddata[1];
        var counter =nameAndScore.datalength;
        var idName = nameAndScore.idName2;
        var idScore = nameAndScore.idScore2;
        // write to modal/show highscore
        for(var i=0; i<counter; i++){
            $(idName[i]).html(names[i]); 
            $(idScore[i]).html(scores[i]);
        }
        nameHighTest = "Name = " + names[0] + '<br>' + 'Score = ' + scores[0];     
        // show it on home page
        $("#demo").html(nameHighTest); 

        // delete typed in name in input field
        $('#nameID').val("");
        
        question = 1;
        points = 0;
        //$('.quiz-result').hide(500);
        $('.progress-bar').css('width', 0+'%').attr('aria-valuenow', 0);
        $('.progress').addClass('hidden');
        // remove the save result button
        $("#saveButton").addClass('hidden');
    });

// clear local storage, for debugging
    $('#clear_button').click(function(){
        localStorage.clear();
        // clear highscore table and best player info
        $("#demo").html("");
        // reset tables
        var idName=[];
        var idScore=[];
        var idName2=[];
        var idScore2=[];
        for(var i=0; i<10; i++){
            var j = i+1;
            idName[i] = "#playerName"+j;
            $(idName[i]).html(""); 
            idScore[i] = "#playerScore"+j;
            $(idScore[i]).html("");
            idName2[i] = "#playerName"+j+j;
            $(idName2[i]).html(""); 
            idScore2[i] = "#playerScore"+j+j;
             $(idScore2[i]).html("");
        } 
    });

    // HighScore Modal Button
    $('#highScoreBtn').click(function(){
        $('#highScoreModal').modal(); // MODAL can also be called with jQuery, like this
            // prepare for showing highscore
                var loadPlayerSort = localStorage.getItem('my_playerData');
                if(loadPlayerSort != null){
                    var nameAndScore = prepareNameScoreModals();
                    var names = nameAndScore.sorteddata[0];
                    var scores = nameAndScore.sorteddata[1];
                    var counter =nameAndScore.datalength;
                    var idName = nameAndScore.idName;
                    var idScore = nameAndScore.idScore;

                    for(var i=0; i<counter; i++){
                        $(idName[i]).html(names[i]); 
                        $(idScore[i]).html(scores[i]);
                    }  
                }else{
                    alert("No highscore to be shown!");
                } 
    });

});
// Function for preparing data to modals
function prepareNameScoreModals(){
                var sorteddata = sortLocalStorage();
                var len =sorteddata[1].length;
                var topTen=10;
                var counter;
                var idName=[];
                var idScore=[];
                var idName2=[];
                var idScore2=[];
                if(len < topTen){
                    counter=len;
                } else{
                    counter=topTen;
                } 
                for(var i=0; i<counter; i++){
                    var j = i+1;
                    idName[i] = "#playerName"+j;
                    idScore[i] = "#playerScore"+j;
                    idName2[i] = "#playerName"+j+j;
                    idScore2[i] = "#playerScore"+j+j;
                }
                var datalength=counter;
                return {sorteddata: sorteddata, datalength: datalength, idName: idName,idScore: idScore,idName2: idName2,idScore2: idScore2}; 
}

// load localStorage and sort names and scores
function sortLocalStorage(){
          var loadPlayerSort = localStorage.getItem('my_playerData');
            if(loadPlayerSort == null){
                alert('no saved results!');
            } 
            else{
                playerDataSort = JSON.parse(loadPlayerSort);
                var playerArrayForSort=[];
                $.each(playerDataSort, function(i,val){  
                    if(val['name']!=="undefined"){
                        playerArrayForSort[i]=[val['name'], val['score']];
                    }              
                });

                playerArrayForSort.sort(sortIt);    // call sort function
     
                // set arrays
                var names = [];
                var scores = [];
                var leng =playerArrayForSort.length;
                for (var j=0; j<leng; j++){
                    names[j] = playerArrayForSort[j][0];
                    scores[j] = playerArrayForSort[j][1]
                }  
                      
                 //return sortHighScore;
                 return [names,  scores]; 
            }
}

// A sort function
function sortIt(elementOne, elementTwo) {
            if(elementOne[1] > elementTwo[1]){ // [1] means it sorts according to score.. [0] would be according to names
               return -1; // if elementOne[1] is higher than elementTwo[1] then return -1, which will gives elementOne a lower index
            }
            else if(elementOne[1] < elementTwo[1]) {
                return 1; // if elementOne[1] is less than elementTwo[1] then return 1, which will gives elementOne a higher index
            }
            else{
                 return 0; // if they are equal return 0, dont change index according to each other
            }
    }   

var player;
function onYouTubeIframeAPIReady() {
            player = new YT.Player('movie', {
              height: '400',
              width: '600',
              videoId: 'hWBrwS-4Kc8',
              events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
              }
            });
}
   
     // what to happen when player is ready and playBtn is pushed
function onPlayerReady(event) {
  
        // bind events
            $(document).ready(function(){
                $("#play-button").click(function(){
                    player.seekTo(0); // prepare clip to start from beginning
                    player.playVideo();
                });
            });
}

        // when video ends test ready show, hide movie
function onPlayerStateChange(event) {        
            if(event.data == 0) {  
                $(document).ready(function(){ 
                    $('.quiz-ready').show(500);
                    $('.movie-ready').hide(500);
                });   
            }
}